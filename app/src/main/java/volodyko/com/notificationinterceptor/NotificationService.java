package volodyko.com.notificationinterceptor;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by volodyko on 23.01.17.
 */

public class NotificationService extends NotificationListenerService {
    private static final String TAG = "NotificationService";
    private Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        String pack = sbn.getPackageName();
        CharSequence ticker = sbn.getNotification().tickerText;
        Bundle extars = sbn.getNotification().extras;
        String title = extars.getString("android.title");
        CharSequence text = extars.getCharSequence("android.text");

        PendingIntent intent = sbn.getNotification().contentIntent;


        Log.i(TAG, "onNotificationPosted: " + pack);
        Log.i(TAG, "onNotificationPosted: " + ticker);
        Log.i(TAG, "onNotificationPosted: " + title);
        Log.i(TAG, "onNotificationPosted: " + text);

        Intent msgRcv = new Intent(MainActivity.FILTER);
        msgRcv.putExtra("extra", extars);
        msgRcv.putExtra("package", pack);
        msgRcv.putExtra("ticker", ticker);
        msgRcv.putExtra("title", title);
        msgRcv.putExtra("text", text);
        msgRcv.putExtra("intent", intent);

        LocalBroadcastManager.getInstance(context).sendBroadcast(msgRcv);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cancelNotification(sbn.getKey());
        } else {
            cancelNotification(sbn.getPackageName(), sbn.getTag(), sbn.getId());
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        super.onNotificationRemoved(sbn);
        Log.d(TAG, "onNotificationRemoved: " + sbn.toString());
    }


}
